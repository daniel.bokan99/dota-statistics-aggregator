package main

import (
	"context"
	"database/sql"
	"encoding/json"
	"errors"

	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
	"github.com/prometheus/client_golang/prometheus"
	"go.etcd.io/etcd/clientv3"

	//	"github.com/prometheus/client_golang/prometheus/promauto"

	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/satori/go.uuid"
	cmn "gitlab.com/leapbit-practice/dota-statistics-common"
)

var config cmn.Config

type Pair struct {
	id         int
	appearance int
}
type aggData struct {
	NbrMatches  int    `json:"nbrMatches"`
	MinId       int    `json:"minId"`
	MaxId       int    `json:"maxId"`
	MinDuration int    `json:"minDuration"`
	MaxDuration int    `json:"maxDuration"`
	AvgDuration int    `json:"avgDuration"`
	RadiantWins int    `json:"radiantWins"`
	DireWins    int    `json:"direWins"`
	Top3Radiant string `json:"top3Radiant"`
	Top3Dire    string `json:"top3Dire"`
}

type Data struct {
	NumOfAgg  int       `json:"NumOfAgg"`
	StartTime time.Time `json:"StartTime"`
	EndTime   time.Time `json:"EndTime"`
}

const (
	sql_appWorktimeQuery = `INSERT INTO app_worktime (uuid, start_time, end_time) VALUES ($1, $2, $3)`
	sql_maxPeriodQuery   = `SELECT COUNT(*) FROM session`
	sql_sessionInsert    = `INSERT INTO session (start_time, end_time) VALUES ($1, $2)`
	sql_endTimeSelect    = `SELECT end_time FROM session WHERE period_id = (SELECT MAX(period_id) FROM session)`
	sql_dotaDataInsert   = `INSERT INTO dota_data (number_of_matches, min_id, max_id, min_duration, max_duration, avg_duration, radiant_wins, dire_wins, top3_radiant, top3_dire, time) 
							VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11);`
	sql_metadataQuery = `SELECT COUNT(*), (SELECT start_time FROM session WHERE period_id = (SELECT MIN(period_id) FROM session)), (SELECT end_time FROM session WHERE period_id = (SELECT MAX(period_id) FROM session))
						 FROM dota_data`
	sql_dataForDayQuery = `SELECT number_of_matches, min_id, max_id, min_duration, max_duration, avg_duration, radiant_wins, dire_wins, top3_radiant, top3_dire
						   from dota_data
					 	   where EXTRACT(HOUR FROM time) BETWEEN 0 AND 23 and EXTRACT(DAY FROM time) = $1 and EXTRACT(MONTH FROM time) = $2 and EXTRACT(YEAR FROM time) = $3`
)
const (
	http_metadataRequestTemplate = "http://%s:80/metadata"
	http_timeRequestTemplate     = "http://%s:80/matchdata?start_time=%s&end_time=%s"
)

var (
	httpReqs = prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "http_requests_total",
		Help: "How many HTTP requests were called",
	},
		[]string{"request", "method"})

	metadataReqCounter = httpReqs.WithLabelValues("metadata", "GET")
	dataForDayCounter  = httpReqs.WithLabelValues("dataForDay", "GET")

	httpTimeResp = prometheus.NewHistogramVec(prometheus.HistogramOpts{
		Name:    "http_time_duration",
		Help:    "Duration of HTTP reponse",
		Buckets: prometheus.LinearBuckets(10, 10, 5),
	},
		[]string{"request", "method"})

	metadataReqTimer = httpTimeResp.WithLabelValues("metadata", "GET")
	dataForDayTimer  = httpTimeResp.WithLabelValues("dataForDay", "GET")

	timer  = prometheus.NewTimer(metadataReqTimer)
	timer2 = prometheus.NewTimer(dataForDayTimer)
)

func httpInit() *http.Client {
	r := mux.NewRouter()
	r.HandleFunc("/metadata", metadataHandler)
	r.HandleFunc("/dataforday", datafordayHandler)
	r.Handle("/metrics", promhttp.Handler())

	go func() {
		fmt.Println("Server started")
		address := config.ServerPort
		log.Fatal(http.ListenAndServe(address, r))
	}()

	myClient := &http.Client{
		Transport:     nil,
		Timeout:       0,
		CheckRedirect: nil,
	}
	return myClient
}

func uuidGenerator() (string, time.Time) {
	appId := uuid.Must(uuid.NewV4()).String()
	appStart := time.Now()
	return appId, appStart
}

func getData(firstParameter string, secondParameter string, db *sql.DB, myClient *http.Client) error {

	timeIntervalRequest := fmt.Sprintf(http_timeRequestTemplate, config.Ip, firstParameter, secondParameter)
	timeIntervalResponse, err := myClient.Get(timeIntervalRequest)
	if err != nil {
		return errors.New("Error while fetching data from metadata parameters")
	}
	body, err := ioutil.ReadAll(timeIntervalResponse.Body)
	if err != nil {
		return errors.New("Error while reading body fetched from metadata parameters")
	}
	var dota []cmn.Match
	err = json.Unmarshal([]byte(body), &dota)
	if err != nil {
		return errors.New("Error while unmarshalling json")
	}
	timeIntervalResponse.Body.Close()
	first, err := strconv.Atoi(firstParameter)
	if err != nil {
		return errors.New("Error while using strconv.Atoi")
	}
	second, err := strconv.Atoi(secondParameter)
	if err != nil {
		return errors.New("Error while using strconv.Atoi")
	}

	t := time.Unix(int64(first), 0)
	rounded := time.Date(t.Year(), t.Month(), t.Day(), t.Hour(), 0, 0, 0, t.Location())
	t1 := rounded.Unix()

	t = time.Unix(int64(second), 0)
	rounded = time.Date(t.Year(), t.Month(), t.Day(), t.Hour()+1, 0, 0, 0, t.Location())
	t2 := rounded.Unix()

	for i := t1; i < t2; i = i + 3600 {
		var dataForAgg []cmn.Match
		for _, v := range dota {
			if v.StartTime >= int(i) && v.StartTime < int(i+3600) {
				dataForAgg = append(dataForAgg, v)
			}
		}
		aggregate(dataForAgg, int(i), db)

		_, err = db.Exec(sql_sessionInsert, time.Unix(int64(i), 0), time.Unix(int64(i+3600), 0))
		if err != nil {
			log.Println("Error while executing sql_sessionInsert query, err: ", err, time.Now())
			continue
		}
	}
	return nil
}

func etcdClientInit() *clientv3.Client {
	stringReq := fmt.Sprintln(config.Ip + ":2379")
	fmt.Println(stringReq)
	etcd, err := clientv3.New(clientv3.Config{
		Endpoints:   []string{stringReq},
		DialTimeout: 5 * time.Second,
	})
	if err != nil {
		log.Println("Error while setting up etcd client, err: ", err)
	}

	return etcd
}

func main() {

	prometheus.MustRegister(httpReqs)
	prometheus.MustRegister(httpTimeResp)

	appId, appStart := uuidGenerator()

	//SETTING UP DATABASE
	db, err := cmn.DatabaseInit()
	if err != nil {
		log.Fatalln("Error")
	}

	config = cmn.GetConfig()

	//SERVER/CLIENT INITIALIZATION
	myClient := httpInit()

	//SIGINT INITIALIZATION
	cmn.SigInit(db, appId, appStart, sql_appWorktimeQuery)

	go func() {
		stringReq := fmt.Sprintf("http://%s:2379", config.Ip)
		fmt.Println(stringReq)
		ctx, _ := context.WithTimeout(context.Background(), 20*time.Second)
		etcd, err := clientv3.New(clientv3.Config{
			Endpoints:   []string{stringReq},
			DialTimeout: 5 * time.Second,
		})
		if err != nil {
			log.Println("Error while setting up etcd client, err: ", err)
		}

		getResponse, err := etcd.Get(ctx, "/match_record/number_18")
		if err != nil {
			fmt.Println("Err while getting key value", err)
		} else {
			fmt.Println(string(getResponse.Kvs[0].Value))
		}

		etcdWatchKey := flag.String("etcdWatchKey", "/match_record/number_", "etcd key to watch")
		flag.Parse()

		watchChan := etcd.Watch(context.Background(), *etcdWatchKey, clientv3.WithPrefix())
		fmt.Println("set WATCH on " + *etcdWatchKey)

		var strSlice []string

		for watchResp := range watchChan {
			//fmt.Println(i, "i je ovolko")
			for _, event := range watchResp.Events {
				//fmt.Printf("Event received! %s executed on %q with value %q\n", event.Type, event.Kv.Key, event.Kv.Value)
				changedKey := string(event.Kv.Value)
				strSlice = append(strSlice, changedKey)
				fmt.Printf("%T, %s\n", changedKey, changedKey)
			}
		}

		fmt.Println(strSlice)

		etcd.Close()
	}()

	var period_count int
	err = db.QueryRow(sql_maxPeriodQuery).Scan(&period_count)
	if err != nil {
		log.Println("Error while executing sql_maxPeriodQuery, err: ", err, time.Now())
	}

	if period_count == 0 {
		// Importing meta data from AppA if its being run for the first time
		http_metadataRequest := fmt.Sprintf(http_metadataRequestTemplate, config.Ip)
		metadataResponse, err := myClient.Get(http_metadataRequest)
		if err != nil {
			log.Fatalln("Error while fetching metadata, err: ", err, time.Now())
		}
		body, err := ioutil.ReadAll(metadataResponse.Body)
		if err != nil {
			log.Fatalln("Error while reading metadata body, err: ", err, time.Now())
		}

		var meta cmn.Metadata
		err = json.Unmarshal([]byte(body), &meta)
		if err != nil {
			log.Fatalln("Error while unmarshalling json, err: ", err, time.Now())
		}
		metadataResponse.Body.Close()

		firstParameter := strconv.Itoa(meta.MinStartTime)
		secondParameter := strconv.Itoa(meta.MaxStartTime)

		err = getData(firstParameter, secondParameter, db, myClient)
		if err != nil {
			log.Fatalln("Error: ", err, time.Now())
		}
	}
	//Infinite loop, every hour getting data from 2 hours ago
	for {

		_, minutes, _ := time.Now().Clock()

		if minutes == 0 {
			var lastInput time.Time
			err = db.QueryRow(sql_endTimeSelect).Scan(&lastInput)
			if err != nil {
				log.Println("Error while executing sql_endTimeSelect query, err: ", err, time.Now())
				time.Sleep(time.Minute)
				continue
			}

			start := strconv.Itoa(int(lastInput.Unix()))
			end := strconv.Itoa(int(time.Now().Local().Add(time.Hour * (-2)).Unix()))

			err = getData(start, end, db, myClient)
			if err != nil {
				log.Println(err, time.Now())
				time.Sleep(time.Minute)
				continue
			}
			fmt.Println("Aggregation sucessful")
			time.Sleep(time.Minute * 2)
		} else {
			fmt.Println("Waiting...")
			time.Sleep(time.Second * 30)
		}
	}
}

func aggregate(dota []cmn.Match, t int, db *sql.DB) {

	var numberOfMatches, radiantWins, direWins, durationSum, minId, minDuration, maxId, maxDuration int
	var minIdSet, maxIdSet, minDurationSet, maxDurationSet bool
	radiantTeamAppearance := make(map[int]int)
	direTeamAppearance := make(map[int]int)

	for _, v := range dota {
		numberOfMatches++
		if !maxIdSet || v.MatchId > maxId {
			maxId = v.MatchId
			maxIdSet = true
		}
		if !minIdSet || v.MatchId < minId {
			minId = v.MatchId
			minIdSet = true
		}
		if v.RadiantWin == true {
			radiantWins++
		} else {
			direWins++
		}
		if !maxDurationSet || v.Duration > maxDuration {
			maxDuration = v.Duration
			maxDurationSet = true
		}
		if !minDurationSet || v.Duration < minDuration {
			minDuration = v.Duration
			minDurationSet = true
		}

		durationSum += v.Duration
		radiantTeamString := strings.Split(v.RadiantTeam, ",")
		direTeamString := strings.Split(v.DireTeam, ",")

		radiantTeam := make([]int, len(radiantTeamString))
		direTeam := make([]int, len(direTeamString))

		for i, v := range radiantTeamString {
			radiantTeam[i], _ = strconv.Atoi(v)
		}
		for i, v := range direTeamString {
			direTeam[i], _ = strconv.Atoi(v)
		}
		for _, v := range radiantTeam {
			radiantTeamAppearance[v]++
		}
		for _, v := range direTeam {
			direTeamAppearance[v]++
		}
	}

	top1Radiant := Pair{0, 0}
	top2Radiant := Pair{0, 0}
	top3Radiant := Pair{0, 0}
	for k, v := range radiantTeamAppearance {
		if v > top1Radiant.appearance {
			top3Radiant = top2Radiant
			top2Radiant = top1Radiant
			top1Radiant.id = k
			top1Radiant.appearance = v
		} else if v > top2Radiant.appearance {
			top3Radiant = top2Radiant
			top2Radiant.id = k
			top2Radiant.appearance = v
		} else if v > top3Radiant.appearance {
			top3Radiant.id = k
			top3Radiant.appearance = v
		}
	}

	top1Dire := Pair{0, 0}
	top2Dire := Pair{0, 0}
	top3Dire := Pair{0, 0}
	for k, v := range direTeamAppearance {
		if v > top1Dire.appearance {
			top3Dire = top2Dire
			top2Dire = top1Dire
			top1Dire.id = k
			top1Dire.appearance = v
		} else if v > top2Dire.appearance {
			top3Dire = top2Dire
			top2Dire.id = k
			top2Dire.appearance = v
		} else if v > top3Dire.appearance {
			top3Dire.id = k
			top3Dire.appearance = v
		}
	}

	top3RadiantAppearance := strconv.Itoa(top1Radiant.id) + "," + strconv.Itoa(top2Radiant.id) + "," + strconv.Itoa(top3Radiant.id)
	top3DireAppearance := strconv.Itoa(top1Dire.id) + "," + strconv.Itoa(top2Dire.id) + "," + strconv.Itoa(top3Dire.id)

	var avg_duration int
	if numberOfMatches != 0 {
		avg_duration = durationSum / numberOfMatches
	}
	if !(minIdSet && maxIdSet && minDurationSet && maxDurationSet) {
		minId = 0
		minDuration = 0
		maxId = 0
		maxDuration = 0
	}
	minIdString := strconv.Itoa(minId)
	maxIdString := strconv.Itoa(maxId)

	_, err := db.Exec(sql_dotaDataInsert, numberOfMatches, minIdString, maxIdString, minDuration, maxDuration, avg_duration, radiantWins, direWins, top3RadiantAppearance, top3DireAppearance, time.Unix(int64(t), 0))
	if err != nil {
		log.Println("Error while executing sql_dotaDataInsert query, err: ", err, time.Now())
		return
	}
	return
}

func metadataHandler(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Content-type", "application-json")
	metadataReqCounter.Inc()
	timer.ObserveDuration()
	db, err := cmn.DatabaseInit()

	var metadata Data
	err = db.QueryRow(sql_metadataQuery).Scan(&metadata.NumOfAgg, &metadata.StartTime, &metadata.EndTime)
	if err != nil {
		log.Println("Error while executing sql_metadataQuery query, err: ", err, time.Now())
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("500 - Something bad happened!"))
		return
	}

	j, err := json.Marshal(metadata)
	if err != nil {
		log.Println("Error while parsing metadata to json, err: ", err, time.Now())
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("500 - Something bad happened!"))
		return
	}
	db.Close()
	w.Write(j)
}

func datafordayHandler(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Content-Type", "application-json")
	dataForDayCounter.Inc()
	timer2.ObserveDuration()
	db, err := cmn.DatabaseInit()

	dateArr, ok := req.URL.Query()["date"]
	if !ok || len(dateArr) == 0 {
		fmt.Println("Invalid request")
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("400 - Invalid request!"))
		return
	}
	dateStr := strings.Join(dateArr, " ")
	date, err := strconv.Atoi(dateStr)
	if err != nil {
		log.Println("Error while using strconv.Atoi, err: ", err, time.Now())
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("500 - Something bad happened!"))
		return
	}
	dateTime := time.Unix(int64(date), 0)
	year, month, day := dateTime.Date()
	monthInt := int(month)

	var data []aggData
	rows, err := db.Query(sql_dataForDayQuery, day, monthInt, year)
	if err != nil {
		log.Println("Error while executing sql_dataForDay query, err: ", err, time.Now())
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("500 - Something bad happened!"))
		return
	}

	for rows.Next() {
		var dataInstance aggData
		err = rows.Scan(&dataInstance.NbrMatches, &dataInstance.MinId, &dataInstance.MaxId, &dataInstance.MinDuration, &dataInstance.MaxDuration, &dataInstance.AvgDuration, &dataInstance.RadiantWins,
			&dataInstance.DireWins, &dataInstance.Top3Radiant, &dataInstance.Top3Dire)
		if err != nil {
			log.Println("Error while scanning rows, err: ", err, time.Now())
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte("500 - Something bad happened!"))
			return
		}
		data = append(data, dataInstance)
	}

	rows.Close()

	j, err := json.Marshal(data)
	if err != nil {
		log.Println("Error while parsing data to json, err: ", err, time.Now())
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("500 - Something bad happened!"))
		return
	}
	db.Close()
	w.Write(j)
}
